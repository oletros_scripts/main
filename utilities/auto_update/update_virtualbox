#!/bin/bash
# root: y

grey="\\e[1;90m"
red="\\e[1;91m"
green="\\e[1;92m"
orange="\\e[1;93m"
blue="\\e[1;94m"
purple="\\e[1;95m"
cyan="\\e[1;96m"
white="\\e[1;97m"
end="\\e[0;0m"

rndstr=`tr -dc "A-Z0-9" < /dev/urandom | head -c10`

############

error_msg() {
	echo -e "$red""$1""$end"
	exit 1
}

status_msg() {
	echo -e "$blue""\n[~] $1""$end"
}

success_msg() {
	echo -e "$green""$1""$end"
}

warning_msg() {
	echo -e "$orange""$1""$end"
}

info_msg() {
	echo -e "$cyan""\t~> $1""$end"
}

############

usage() {
	echo -e "$purple
Usage: $0 (-f) $end
Details:
-$cyan -f$end: force install/reinstall/update
$end"
	exit 0
}

if [ "$1" == "--help" ] || [ "$1" == "-h" ]; then
	usage
fi

############

isforced=false

while getopts f flag
do
	case "${flag}" in
		f) isforced=true;;
	esac
done

############

status_msg "Initialize"

stusr=`cat /etc/passwd | grep "1000:1000"`

user=`echo "$stusr" | cut -d':' -f1`
userhome=`echo "$stusr" | cut -d':' -f6`

############

latest() {
	info_msg "Get latest deb"
	
	link=`curl -L -s "https://www.virtualbox.org/wiki/Linux_Downloads" | grep "Debian 12" | cut -d'"' -f4`
	version=`echo "$link" | cut -d'/' -f5 | cut -d'-' -f2- | cut -d'~' -f1`
	
	wget -q --show-progress "$link" -O "/tmp/$rndstr.deb"
	dpkg -i "/tmp/$rndstr.deb"
	
	echo "$version" > /usr/local/share/auto_install/virtualbox/current.version
	
	info_msg "Get corresponding addon"
	
	addonname=`curl -L -s "https://download.virtualbox.org/virtualbox/$version/" | grep ".vbox-extpack" | head -1 | cut -d'"' -f2`
	addonlnk="https://download.virtualbox.org/virtualbox/7.1.0/$addonname"
	
	wget -q --show-progress "$addonlnk" -O "$userhome/virtualbox/addons/$addonname"
	
	chown $user:$user "$userhome/virtualbox/addons/$addonname"
}

installing() {
	info_msg "Remove actual version if exists"
	
	apt update
	apt remove -y --force-yes virtualbox*
	
	info_msg "Create mandatory folders"
	
	if [ ! -d /usr/local/share/auto_install/virtualbox ]; then
		mkdir -p /usr/local/share/auto_install/virtualbox
	fi
	
	if [ ! -d "$userhome/virtualbox/addons" ]; then
		mkdir -p "$userhome/virtualbox/addons"
		
		chown -R $user:$user "$userhome/virtualbox/addons"
	fi
	
	apt install libxcb-cursor0
	
	latest
}

############

status_msg "Check current install"

installed=false
latest=false

if [ -e /usr/local/share/auto_install/virtualbox/current.version ]; then
	success_msg "Installed!"
	installed=true
	
	lastversion=`curl -L -s "https://www.virtualbox.org/wiki/Linux_Downloads" | grep "Debian 12" | cut -d'"' -f4 | cut -d'/' -f5 | cut -d'-' -f2- | cut -d'~' -f1`
	currentversion=`cat /usr/local/share/auto_install/virtualbox/current.version`
	
	if [ "$lastversion" != "$currentversion" ]; then
		warning_msg "Not latest version!"
		latest=false
	else
		success_msg "Latest version!"
		latest=true
	fi
else
	warning_msg "Not installed and/or properly!"
	installed=false
fi

############

if ! ($installed); then
	installing
fi

############

if ! ($latest); then
	latest
fi

############

if ($isforced); then
	info_msg "Force update virtualbox"
	
	latest
fi

############

status_msg "Clean traces"

if [ -e "/tmp/$rndstr.deb" ]; then
	rm "/tmp/$rndstr.deb"
fi

############

status_msg "Done"
exit 0
