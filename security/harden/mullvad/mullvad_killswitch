#!/bin/bash
# root: y

############

# Old Values

# old backup DNS: 193.138.218.74
# old api port: 1234

############

grey="\\e[1;90m"
red="\\e[1;91m"
green="\\e[1;92m"
orange="\\e[1;93m"
blue="\\e[1;94m"
purple="\\e[1;95m"
cyan="\\e[1;96m"
white="\\e[1;97m"
end="\\e[0;0m"

rnd=`tr -dc "a-z0-9" < /dev/urandom | head -c16`

############

error_msg() {
	echo -e "$red""$1""$end"
	exit 1
}

status_msg() {
	echo -e "$blue""\n[~] $1""$end"
}

info_msg() {
	echo -e "$purple""\n\t~>$1""$end"
}

success_msg() {
	echo -e "$green""$1""$end"
}

failure_msg() {
	echo -e "$red""$1""$end"
}

warning_msg() {
	echo -e "$orange""$1""$end"
}

debug_msg() {
	echo -e "$cyan""$1""$end"
}

############

need_root() {
	if [ "$UID" != "0" ]; then
		zenity --error --title "Mullvad KillSwitch - Setup" --text "Root is needed using su. Run this script as root!" 2>/dev/null
		exit 1
	fi
}

need_root

############

install_dep() {
	checkdep=`which iptables`
	if [ "$checkdep" == "" ]; then
		apt install iptables
		
	fi
	
	checkjq=`dpkg -s jq | grep 'install ok installed'`
	if [ "$checkjq" == "" ]; then
		apt install jq
	fi
}

install_dep

############

get_user_agent() {
	uadefault=false
	uaff=""
	useragent=""
	
	thehome=`cat /etc/passwd | grep '1000:1000' | cut -d':' -f6`
	
	if [ "$thehome" != "" ]; then
		checkesrfolder=`find $thehome/.mozilla/firefox -type d -name '*.default-esr' | head -1`
		checknightlyfolder=`find $thehome/.mozilla/firefox -type d -name '*.default-nightly' | head -1`
		
		if [ "$checkesrfolder" != "" ]; then
			uadefault=false
			uaff="esr"
		elif [ "$checkesrfolder" != "" ]; then
			uadefault=false
			uaff="nightly"
		else
			uadefault=true
		fi
		
		if [ "$uaff" == "esr" ]; then
			esrprefs="$checkesrfolder/prefs.js"
			esrchekuaprefs=`cat "$esrprefs" | grep 'user_pref("devtools.responsive.userAgent"' | cut -d'"' -f4`
			
			if [ "$esrchekuaprefs" != "" ]; then
				useragent="$esrchekuaprefs"
			else
				uadefault=true
			fi
		elif [ "$uaff" == "nightly" ]; then
			nightlyprefs="$checknightlyfolder/prefs.js"
			nightlychekuaprefs=`cat "$nightlyprefs" | grep 'user_pref("devtools.responsive.userAgent"' | cut -d'"' -f4`
			
			if [ "$nightlychekuaprefs" != "" ]; then
				useragent="$nightlychekuaprefs"
			else
				uadefault=true
			fi
		else
			uadefault=true
		fi
	fi
	
	if ($uadefault) || [ "$useragent" == "" ] || [ "$thehome" == "" ]; then
		useragent='Mozilla/5.0 (Windows NT 10.0; rv:109.0) Gecko/20100101 Firefox/115.0'
	fi
	
	echo "$useragent"
}

useragent=`get_user_agent`

############

usage() {
	echo -e "$purple
Usage: $0 [-c] [-f] [-l 'lo'] [-p 'tun'] [-a] [-u 'wlan1,wlan2'] $end
Details:
-$cyan -c$end: console mode. You need to provide all others parameters if so.
-$cyan -f$end: force mode if the killswitch already exists. If not provided and if already exists, the script will exit on error.
-$cyan -l 'lo'$end: specify the card name for localhost. Default value is 'lo' if nothing provided.
-$cyan -p 'tun'$end: specify the card name for vpn tunnel. Default value is 'tun' if nothing provided.
-$cyan -a$end: to add all existing cards & their ifnames values to the killswitch.
-$cyan -u 'wlan1,wlan2'$end: to add custom card names separated by commas.
$end"
	exit 0
}

if [ "$1" == "--help" ] || [ "$1" == "-h" ]; then
	usage
fi

############

isconsole=false
isforced=false
islocalhost=false
isvpntunnel=false
isallexistingcard=false
isspecificcard=false

while getopts cfal:p:u: flag
do
	case "${flag}" in
		c) isconsole=true;;
		f) isforced=true;;
		l) islocalhost=true; cslocalhost=${OPTARG};;
		p) isvpntunnel=true; csvpntunnel=${OPTARG};;
		a) isallexistingcard=true;;
		u) isspecificcard=true; specificcards=${OPTARG};;
	esac
done

if ($isspecificcard) && [ "$specificcards" == "" ]; then
	error_msg "Cards are not found! Follow the usage!"
fi

if ($islocalhost) && [ "$cslocalhost" == "" ]; then
	cslocalhost="lo"
fi

if ($isvpntunnel) && [ "$csvpntunnel" == "" ]; then
	csvpntunnel="tun"
fi

############

get_openvpn_list_ip() {
	status_msg "Get all relays"
	
	curl -L -s -A "$useragent" "https://api.mullvad.net/www/relays/all/" > /tmp/relays_$rnd.txt
	
	status_msg "Extract good ones"
	
	ips=""
	ipbs=""
	i=1

	while (true)
	do
		info_msg "Entry $i"
		
		active=`cat /tmp/relays_$rnd.txt | jq .[$i].active | sed 's;";;g'`
		type=`cat /tmp/relays_$rnd.txt | jq .[$i].type | sed 's;";;g'`
		hostnamebr=`cat /tmp/relays_$rnd.txt | jq .[$i].hostname | sed 's;";;g' | grep -Eo "\-.*\-br\-.*"`
		
		if [ "$hostnamebr" != "" ] && [ "$type" == "bridge" ]; then
			debug_msg "Bridge"
			
			actualipb=`cat /tmp/relays_$rnd.txt | jq .[$i].ipv4_addr_in | sed 's;";;g'`
			
			if [ "$ipbs" == "" ]; then
				ipbs="$actualipb"
			else
				ipbs="$ipbs,$actualipb"
			fi
		fi
		
		if [ "$active" == "true" ] && [ "$type" == "openvpn" ]; then
			success_msg "OK"
			
			actualip=`cat /tmp/relays_$rnd.txt | jq .[$i].ipv4_addr_in | sed 's;";;g'`
			
			if [ "$ips" == "" ]; then
				ips="$actualip"
			else
				ips="$ips,$actualip"
			fi
		elif [ "$active" == "null" ]; then
			warning_msg "End"
			break
		else
			failure_msg "KO"
		fi
		
		let i=i+1
	done
	
	status_msg "Save the result"
	
	echo "$ips" > /tmp/mullvad_ips_$rnd.txt
	echo "$ipbs" > /tmp/mullvad_ipbs_$rnd.txt
}

get_openvpn_list_ip

############

ipbs=`cat "/tmp/mullvad_ipbs_$rnd.txt"`
ipaddresses=`cat "/tmp/mullvad_ips_$rnd.txt"`

# Ports are not supposed to be changed
portsudp="53,1194,1195,1196,1197,1300,1301,1302"
portstcp="53,443"

portstcpapi=""

# doh.mullvad.net has address 194.242.2.2
dohip=`dig A +noall +answer doh.mullvad.net | sed '/CNAME/d' | grep -Eo '[0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3}'`
if [ "$dohip" != "194.242.2.2" ]; then
	error_msg "doh.mullvad.net has changed his IP. Check on Mullvad if it's normal and/or always in used!"
fi
iptcpapi="$ipbs,194.242.2.2"

############

remove_existing() {
	rm -f /etc/init.d/killswitch
	update-rc.d killswitch remove
}

############

if ($isconsole); then
	if [ -e /etc/init.d/killswitch ]; then
		if ($isforced); then
			remove_existing
		else
			error_msg "Killswitch found already! Forced parameter need to be passed in console mode to overwrite!"
		fi
	fi
else
	if [ -e /etc/init.d/killswitch ]; then
		zenity --error --title "Mullvad KillSwitch - Setup" --text "Script already setup!" --width 300 --height 150 2>/dev/null
		choixscript=`zenity --list --title "Mullvad KillSwitch - Setup" --text "Do you want to continue?\nIf yes, the actual killswitch will be deleted!" --radiolist --column "Choice" --column "Options" TRUE "Yes" FALSE "No" 2>/dev/null`
		if [ "$choixscript" == "Yes" ]; then
			remove_existing
		elif [ "$choixscript" == "No" ]; then
			exit 0
		else
			exit 1
		fi
	fi
fi
	
touch /etc/init.d/killswitch
chmod +x /etc/init.d/killswitch

info_msg "Initialize"

echo '#!/bin/sh -e

### BEGIN INIT INFO
# Provides:          killswitch
# Required-Start:    $network $remote_fs $syslog
# Required-Stop:     $network $remote_fs $syslog
# Should-Start:      network-manager
# Should-Stop:       network-manager
# X-Start-Before:    $x-display-manager gdm kdm xdm wdm ldm sdm nodm
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Mullvad KillSwitch
# Description:       Mullvad KillSwitch using iptables. IPv6 is totally forbidden.
### END INIT INFO
' > /etc/init.d/killswitch

info_msg "Block IPv6"

echo '# IPv6: KO
ip6tables -P OUTPUT DROP
ip6tables -P INPUT DROP
' >> /etc/init.d/killswitch

if ($isconsole); then
	if [ "$cslocalhost" == "" ]; then
		error_msg "Localhost value not found! Localhost parameter need to be passed in console mode!"
	else
		localhost="$cslocalhost"
	fi
else
	localhost=`zenity --entry --title "Mullvad KillSwitch - Setup" --text "Local interface (default value will be lo):" 2>/dev/null`
	if [ "$localhost" == "" ]; then
		localhost="lo"
	fi
fi

if ($isconsole); then
	if [ "$csvpntunnel" == "" ]; then
		error_msg "VPN Tunnel value not found! VPN Tunnel parameter need to be passed in console mode!"
	else
		vpntunnel="$csvpntunnel"
	fi
else
	vpntunnel=`zenity --entry --title "Mullvad KillSwitch - Setup" --text "VPN interface (default value will be tun):" 2>/dev/null`
	if [ "$vpntunnel" == "" ]; then
		vpntunnel="tun"
	fi
fi

info_msg "Configure localhost & tunnel"

echo "# IPv4: OK
iptables -P OUTPUT DROP
iptables -A OUTPUT -o $vpntunnel+ -j ACCEPT
iptables -A INPUT -i $localhost -j ACCEPT
iptables -A OUTPUT -o $localhost -j ACCEPT
iptables -A OUTPUT -d 255.255.255.255 -j ACCEPT
iptables -A INPUT -s 255.255.255.255 -j ACCEPT
" >> /etc/init.d/killswitch

if ($isconsole); then
	if ($isallexistingcard); then
		info_msg "Add all existing card & IFNAMES values"
		
		for iface in `ifconfig | grep mtu | cut -d':' -f1 | uniq -i`
		do
			if [ "$iface" != "$localhost" ] && [[ $iface != *$vpntunnel* ]]; then
				if [ "$portstcpapi" == "" ]; then
					echo "iptables -A OUTPUT -o $iface -p udp -m multiport --dports $portsudp -d $ipaddresses -j ACCEPT
	iptables -A OUTPUT -o $iface -p tcp -m multiport --dports $portstcp -d $iptcpapi,$ipaddresses -j ACCEPT
	" >> /etc/init.d/killswitch
				else
					echo "iptables -A OUTPUT -o $iface -p udp -m multiport --dports $portsudp -d $ipaddresses -j ACCEPT
	iptables -A OUTPUT -o $iface -p tcp -m multiport --dports $portstcp,$portstcpapi -d $iptcpapi,$ipaddresses -j ACCEPT
	" >> /etc/init.d/killswitch
				fi
				
				success_msg "Interface $iface added"
					
				udevadm test-builtin net_id /sys/class/net/$iface > /tmp/killswitch_$rnd.txt
				ifnamesiface=`cat /tmp/killswitch_$rnd.txt | grep 'ID_NET_NAME_PATH' | cut -d'=' -f2`
					
				if [ "$ifnamesiface" != "" ] && [ "$ifnamesiface" != "$iface" ]; then
					if [ "$portstcpapi" == "" ]; then
						echo "iptables -A OUTPUT -o $ifnamesiface -p udp -m multiport --dports $portsudp -d $ipaddresses -j ACCEPT
	iptables -A OUTPUT -o $ifnamesiface -p tcp -m multiport --dports $portstcp -d $iptcpapi,$ipaddresses -j ACCEPT
	" >> /etc/init.d/killswitch
					else
						echo "iptables -A OUTPUT -o $ifnamesiface -p udp -m multiport --dports $portsudp -d $ipaddresses -j ACCEPT
	iptables -A OUTPUT -o $ifnamesiface -p tcp -m multiport --dports $portstcp,$portstcpapi -d $iptcpapi,$ipaddresses -j ACCEPT
	" >> /etc/init.d/killswitch
					fi
					
					success_msg "Interface (IFNAMES) $ifnamesiface added"
				fi
					
				if [ -e /tmp/killswitch_$rnd.txt ]; then
					rm /tmp/killswitch_$rnd.txt
				fi
			fi
		done
	fi
	
	if ($isspecificcard); then
		info_msg "Add custom cards"
		
		checkcomma=`echo "$specificcards" | grep ','`
		if [ "$checkcomma" == "" ]; then
			iface="$specificcards"
			
			if [ "$portstcpapi" == "" ]; then
				echo "iptables -A OUTPUT -o $iface -p udp -m multiport --dports $portsudp -d $ipaddresses -j ACCEPT
		iptables -A OUTPUT -o $iface -p tcp -m multiport --dports $portstcp -d $iptcpapi,$ipaddresses -j ACCEPT
		" >> /etc/init.d/killswitch
			else
				echo "iptables -A OUTPUT -o $iface -p udp -m multiport --dports $portsudp -d $ipaddresses -j ACCEPT
		iptables -A OUTPUT -o $iface -p tcp -m multiport --dports $portstcp,$portstcpapi -d $iptcpapi,$ipaddresses -j ACCEPT
		" >> /etc/init.d/killswitch
			fi
			
			success_msg "Interface $iface added"
			
			udevadm test-builtin net_id /sys/class/net/$iface > /tmp/killswitch_$rnd.txt
			ifnamesiface=`cat /tmp/killswitch_$rnd.txt | grep 'ID_NET_NAME_PATH' | cut -d'=' -f2`
				
			if [ "$ifnamesiface" != "" ] && [ "$ifnamesiface" != "$iface" ]; then
				if [ "$portstcpapi" == "" ]; then
					echo "iptables -A OUTPUT -o $ifnamesiface -p udp -m multiport --dports $portsudp -d $ipaddresses -j ACCEPT
	iptables -A OUTPUT -o $ifnamesiface -p tcp -m multiport --dports $portstcp -d $iptcpapi,$ipaddresses -j ACCEPT
	" >> /etc/init.d/killswitch
				else
					echo "iptables -A OUTPUT -o $ifnamesiface -p udp -m multiport --dports $portsudp -d $ipaddresses -j ACCEPT
	iptables -A OUTPUT -o $ifnamesiface -p tcp -m multiport --dports $portstcp,$portstcpapi -d $iptcpapi,$ipaddresses -j ACCEPT
	" >> /etc/init.d/killswitch
				fi
					
				success_msg "Interface (IFNAMES) $ifnamesiface added"
			fi
		else
			for iface in `echo "$specificcards" | tr ',' ' '`
			do
				if [ "$portstcpapi" == "" ]; then
					echo "iptables -A OUTPUT -o $iface -p udp -m multiport --dports $portsudp -d $ipaddresses -j ACCEPT
			iptables -A OUTPUT -o $iface -p tcp -m multiport --dports $portstcp -d $iptcpapi,$ipaddresses -j ACCEPT
			" >> /etc/init.d/killswitch
				else
					echo "iptables -A OUTPUT -o $iface -p udp -m multiport --dports $portsudp -d $ipaddresses -j ACCEPT
			iptables -A OUTPUT -o $iface -p tcp -m multiport --dports $portstcp,$portstcpapi -d $iptcpapi,$ipaddresses -j ACCEPT
			" >> /etc/init.d/killswitch
				fi
				
				success_msg "Interface $iface added"
				
				udevadm test-builtin net_id /sys/class/net/$iface > /tmp/killswitch_$rnd.txt
				ifnamesiface=`cat /tmp/killswitch_$rnd.txt | grep 'ID_NET_NAME_PATH' | cut -d'=' -f2`
					
				if [ "$ifnamesiface" != "" ] && [ "$ifnamesiface" != "$iface" ]; then
					if [ "$portstcpapi" == "" ]; then
						echo "iptables -A OUTPUT -o $ifnamesiface -p udp -m multiport --dports $portsudp -d $ipaddresses -j ACCEPT
		iptables -A OUTPUT -o $ifnamesiface -p tcp -m multiport --dports $portstcp -d $iptcpapi,$ipaddresses -j ACCEPT
		" >> /etc/init.d/killswitch
					else
						echo "iptables -A OUTPUT -o $ifnamesiface -p udp -m multiport --dports $portsudp -d $ipaddresses -j ACCEPT
		iptables -A OUTPUT -o $ifnamesiface -p tcp -m multiport --dports $portstcp,$portstcpapi -d $iptcpapi,$ipaddresses -j ACCEPT
		" >> /etc/init.d/killswitch
					fi
						
					success_msg "Interface (IFNAMES) $ifnamesiface added"
				fi
			done
		fi
	fi
else
	while [ "1" = "1" ]
	do

	addinterface=`zenity --width 300 --height 200 --list --title "Mullvad KillSwitch - Setup" --text "Add an network interface" --column Choice "All available" "Custom" "Exit" 2>/dev/null`

	if [ "$addinterface" = "All available" ]; then
		for iface in `ifconfig | grep mtu | cut -d':' -f1 | uniq -i`
		do
			if [ "$iface" != "$localhost" ] && [[ $iface != *$vpntunnel* ]]; then
				if [ "$portstcpapi" == "" ]; then
					echo "iptables -A OUTPUT -o $iface -p udp -m multiport --dports $portsudp -d $ipaddresses -j ACCEPT
	iptables -A OUTPUT -o $iface -p tcp -m multiport --dports $portstcp -d $iptcpapi,$ipaddresses -j ACCEPT
	" >> /etc/init.d/killswitch
				else
					echo "iptables -A OUTPUT -o $iface -p udp -m multiport --dports $portsudp -d $ipaddresses -j ACCEPT
	iptables -A OUTPUT -o $iface -p tcp -m multiport --dports $portstcp,$portstcpapi -d $iptcpapi,$ipaddresses -j ACCEPT
	" >> /etc/init.d/killswitch
				fi
					
				zenity --info --title "Mullvad KillSwitch - Setup" --text "Interface $iface added" 2>/dev/null
					
				udevadm test-builtin net_id /sys/class/net/$iface > /tmp/killswitch_$rnd.txt
				ifnamesiface=`cat /tmp/killswitch_$rnd.txt | grep 'ID_NET_NAME_PATH' | cut -d'=' -f2`
					
				if [ "$ifnamesiface" != "" ] && [ "$ifnamesiface" != "$iface" ]; then
					if [ "$portstcpapi" == "" ]; then
						echo "iptables -A OUTPUT -o $ifnamesiface -p udp -m multiport --dports $portsudp -d $ipaddresses -j ACCEPT
	iptables -A OUTPUT -o $ifnamesiface -p tcp -m multiport --dports $portstcp -d $iptcpapi,$ipaddresses -j ACCEPT
	" >> /etc/init.d/killswitch
					else
						echo "iptables -A OUTPUT -o $ifnamesiface -p udp -m multiport --dports $portsudp -d $ipaddresses -j ACCEPT
	iptables -A OUTPUT -o $ifnamesiface -p tcp -m multiport --dports $portstcp,$portstcpapi -d $iptcpapi,$ipaddresses -j ACCEPT
	" >> /etc/init.d/killswitch
					fi

					zenity --info --title "Mullvad KillSwitch - Setup" --text "Interface (IFNAMES) $ifnamesiface added" 2>/dev/null
				fi
					
				if [ -e /tmp/killswitch_$rnd.txt ]; then
					rm /tmp/killswitch_$rnd.txt
				fi
			fi
		done

	elif [ "$addinterface" = "Custom" ]; then
		iface=`zenity --entry --title "Mullvad KillSwitch - Setup" --text "Interface à ajouter :" 2>/dev/null`
		if [ "$iface" == "" ]; then
			rm /etc/init.d/killswitch
			zenity --error --title "Mullvad KillSwitch - Setup" --text "You had to choose!" --width 300 --height 150 2>/dev/null
			exit 1
		fi
			
		if [ "$portstcpapi" == "" ]; then
			echo "iptables -A OUTPUT -o $iface -p udp -m multiport --dports $portsudp -d $ipaddresses -j ACCEPT
	iptables -A OUTPUT -o $iface -p tcp -m multiport --dports $portstcp -d $iptcpapi,$ipaddresses -j ACCEPT
	" >> /etc/init.d/killswitch
		else
			echo "iptables -A OUTPUT -o $iface -p udp -m multiport --dports $portsudp -d $ipaddresses -j ACCEPT
	iptables -A OUTPUT -o $iface -p tcp -m multiport --dports $portstcp,$portstcpapi -d $iptcpapi,$ipaddresses -j ACCEPT
	" >> /etc/init.d/killswitch
		fi
			
		zenity --info --title "Mullvad KillSwitch - Setup" --text "Interface $iface added" 2>/dev/null

	elif [ "$addinterface" = "Exit" ]; then
		break

	else
		if [ -e /etc/init.d/killswitch ]; then
			rm /etc/init.d/killswitch
		fi
		
		exit 1	
	fi

	done
fi

info_msg "Enable killswitch"

systemctl enable killswitch
systemctl start killswitch

############

if [ -e /tmp/mullvad_ips_$rnd.txt ]; then
	rm /tmp/mullvad_ips_$rnd.txt
fi

if [ -e /tmp/mullvad_ipbs_$rnd.txt ]; then
	rm /tmp/mullvad_ipbs_$rnd.txt
fi

############

if ($isconsole); then
	status_msg "Done"
else
	zenity --info --title "Mullvad KillSwitch - Setup" --text "Done" 2>/dev/null
fi

exit 0
